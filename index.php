<?php

 include 'includes/class-autoload.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="includes/calc.inc.php" method = "post">

    <p>Calculator </p>
    <input type= "number" name="num1" placeholder="First number" required>
    <select name="oper" >
      <option value="add"> + </option>
      <option value="sub"> - </option>
      <option value="div"> / </option>
      <option value="mul"> x </option>
    </select>
    <input type="number" name="num2" placeholder="Second number" required >
    <button type="submit" name="submit">  Calculate</button>

</form>

</body>
</html>